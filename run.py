import argparse
from manager import Manager

if __name__ == '__main__':
    desc = """
    A runnable script for ta management
    """

    parser = argparse.ArgumentParser(description = desc)
    subparser = parser.add_subparsers(help = 'Available options')

    upload_parser = subparser.add_parser('upload', help = 'upload to db')
    upload_parser.set_defaults(task = 'upload')
    upload_parser.add_argument('--run-number', '-r', action = 'store', required = True,
                        help = 'the run number')
    upload_parser.add_argument('--path', '-p', action = 'store', required = True,
                        help = 'absolute path to the ntuple file')
    upload_parser.add_argument('--bxid-ref', '-b', action = 'store', required = True,
                        help = 'bxid reference for incr. ta scan')
    upload_parser.add_argument('--equalisation', '-e', action = 'store', required = True,
                        help = 'equalisation name TA was taken for')
    upload_parser.add_argument('--comment', '-c', action = 'store')
    #upload_parser.add_argument('--which', '-w', action = 'store', const = None, default = None, nargs = '*',
    #                    help = 'a list of local paths to the modified files')
    
    args = parser.parse_args()
    manager = Manager()
    print(args.__dict__)
    if args.__dict__['task'] == 'upload':
        manager.upload_ta(task = args.__dict__)
