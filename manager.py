import os
import requests
import json
import datetime 
import uproot
import numpy as np
import psutil

class Manager:
    def __init__(
        self,
    ):
        """
        Manager class for db interaction
        """
        self.api_host = 'http://10.128.124.243:8003'
        self.storage = '/calib/velo/.ta/'

    def _post(self, apath, data):
        session = requests.Session()
        session.trust_env = False
        content = session.post(
            "{}{}".format(self.api_host, '/api/' + apath),
            data = json.dumps(data),
            verify = False,
        )
        content.raise_for_status()
        return content.json()

    def upload_ta(
        self, 
        task: dict = None
    ):
        t = datetime.datetime.now()

        ### TODO: Move to some sort of initialise (or install)
        if not os.path.isdir(self.storage + task['run_number']):
            os.mkdir(self.storage + task['run_number'])
        ######

        data = {'date': t.strftime('%H:%M:%S %d-%m-%Y'),
                'run': task['run_number'],
                'uploaded_by': os.getlogin(),
                'uploaded_from': os.uname().nodename, 
                'comment': task['comment'],
                'path': task['path'],
                'bxidref': task['bxid_ref'],
                'equalisation': task['equalisation'],}
        
        #POST the recipe
        self._post('ta', data)
        
        #compute ta for each asic
        bxidrange = 5
        accumulated_data = {}
        for mod in range(52):
            print('Computing incr. TA for module ...', mod)
            if not os.path.isdir(self.storage + task['run_number'] + '/Module' + str(mod).zfill(2)):
                os.mkdir(self.storage + task['run_number'] + '/Module' + str(mod).zfill(2))

            with uproot.open(task['path']) as file:
                tree = file.get("VPClusterTuple/ClusterTuple")
                h = tree.arrays(['col','row','asic+3*sensorNb','bxid'], library = 'np', cut = 'module == %d'%(mod))
            process = psutil.Process()
            print(f'Memory usage is {process.memory_info().rss}')
                
            h1 = np.c_[h['row'], h['col'], h['asic+3*sensorNb'], h['bxid'] - int(task['bxid_ref']) + 20]
            hh,_axis = np.histogramdd(h1,(np.arange(0,257),np.arange(0,257),np.arange(12+1),np.arange(-bxidrange+20,bxidrange+1+20)))
            h2 = np.sum(np.sum(hh,axis=0),axis=0)
            #print(h2)

            _axisall = np.arange(-bxidrange+20,bxidrange+20)
            mean_masked = np.ma.sum(hh*_axisall,axis=-1)/np.ma.sum(hh,axis=-1)

            for tile in range (0,4):
                for asic in range (0,3):

                    h = np.full([256,256], np.nan)
                    h[mean_masked.mask[:,:,asic + 3*tile] == False] = mean_masked[:,:,asic + 3*tile][mean_masked.mask[:,:,asic + 3*tile] == False] - 20
                    vpid = str((asic + 3*tile) + 12*mod)
                    mean = np.round(np.nanmean(h, dtype = 'float32'), 2)
                    std = np.round(np.nanstd(h, dtype = 'float32'), 2)
                    min = np.round(np.nanmin(h).astype('float32'), 2)
                    max = np.round(np.nanmax(h).astype('float32'), 2)
                    if not isinstance(mean, np.floating): mean = 0
                    if not isinstance(std, np.floating): std = 0
                    if not isinstance(min, np.floating): min = 0
                    if not isinstance(max, np.floating): max = 0

                    to_save_path = (self.storage + task['run_number'] + '/Module' + str(mod).zfill(2) + '/Module' 
                        + str(mod).zfill(2) + '_VP' + str(tile) + '-' + str(asic) + '_' + task['run_number'] + '_ita')
                    to_save_path_bxid_hist = to_save_path + '_bxid_hist'
                    accumulated_data = {'asic': str((asic + 3*tile) + 12*mod),  #['ta'+ vpid]
                                                    'bxid_mean': str(mean), 
                                                    'bxid_std': str(std), 
                                                    'bxid_min': str(min), 
                                                    'bxid_max': str(max),
                                                    'csv_path': to_save_path + '.csv',
                                                    'bxid_hist_path': to_save_path_bxid_hist + '.csv'}
                    print(accumulated_data)
                    self._post('ta/asic', accumulated_data)
                    np.savetxt(to_save_path + '.csv', h, fmt = '%.3f')
                    np.savetxt(to_save_path_bxid_hist + '.csv', h2[tile*3 + asic], fmt = '%i')

                    
