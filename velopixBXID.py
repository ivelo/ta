import uproot
import numpy as np
from bokeh.plotting import figure, save, output_file, show
from bokeh.embed import components
from bokeh.models import Quad, Step, LinearColorMapper, ColumnDataSource, Select, CustomJS, Slider, LogScale, LinearScale, ColorBar, RangeSlider
from bokeh.layouts import column, row
from bokeh.palettes import diverging_palette
from copy import copy
import time
import json
import requests

#reference for this file 1903

def plotsOneModule(velomodule,asic,sensor,bxidreference,runnumber):
    bxidrange = 5

## make a nice np.array of shape [nevents,4] (4 being col row asicid and bxid)
## hh is the histo of bxid for each pixel (col,row,asic)
    with uproot.open(f"/calib/velo/time_alignment/tuples/runs/{runnumber}/ntuple_279986_000.root") as file:
        tree = file.get("VPClusterTuple/ClusterTuple")
        h = tree.arrays(['col','row','asic+3*sensorNb','bxid'],library = 'np',cut = 'module == %d'%(velomodule))
    h1 = np.c_[h['col'],h['row'],h['asic+3*sensorNb'],h['bxid']-bxidreference+20]
    #h1 = np.append(h1,np.c_[h['col'],h['row'],h['asic+3*sensorNb'],h['bxid']-bxidreference2+20],axis=0)
    hh,_axis = np.histogramdd(h1,(np.arange(0,257),np.arange(0,257),np.arange(12+1),np.arange(-bxidrange+20,bxidrange+1+20)))

# now compute the average and stddev for all the pixels
    _axisall = np.repeat(np.repeat(np.repeat(_axis[3][:-1].reshape(1,_axis[3][:-1].shape[0]),12,axis= 0).reshape(1,12,_axis[3][:-1].shape[0]),256,axis=0).reshape(1,256,12,_axis[3][:-1].shape[0]),256,axis=0)
    mean_masked = np.ma.average(_axisall, weights = hh, axis = -1)   

    print(mean_masked.shape, _axisall.shape)
    #you could calculate stardand deviation
    #meanreshaped = np.repeat(np.expand_dims(mean_masked, axis = -1),_axis[3][:-1].shape[0], axis = -1)
    #std_masked = np.sqrt(np.ma.average(np.power(_axisall-meanreshaped,2), weights=hh, axis = -1))


    h = np.zeros((256,256))
    h[0:256,:]=mean_masked[:,:,sensor*3+asic]-20
    datamap = h
    p = figure(width=565, height=445, x_range = (0,255), y_range = (0,255))
    dw, dh = 255, 255
    datamap_const = copy(datamap)
    src = ColumnDataSource(data = dict(image_now = [datamap], image_const = [datamap_const]))
    c_mapper = LinearColorMapper(palette = 'Turbo256')
    r = p.image(image = 'image_now',source = src, x = 0, y = 0, dw = 255, dh = 255, color_mapper = c_mapper)
    cb_low = RangeSlider(start=-5, end=5, value= (-5,5), 
        step=0.5, title="Colorbar scale", width = 160, show_value = True)

    cb_low.js_link('value', r.glyph.color_mapper, 'low', attr_selector=0)
    cb_low.js_link('value', r.glyph.color_mapper, 'high', attr_selector=1)
   
    p.xaxis.axis_label = 'Pixel column'
    p.xaxis.axis_label_text_font_size = '8pt'
    p.yaxis.axis_label = 'Pixel row'
    p.yaxis.axis_label_text_font_size = '8pt'
    
    cb = r.construct_color_bar(width = 15)
    p.add_layout(cb, 'right')
    
    output_file(filename='bokeh_averageBXID.html', title='Static HTML file')
    save(row(p,column(cb_low)))
    return 0

def TimeAlignmentDict(runnumber,bxidreference):
    bxidrange = 5
    to_return = {}
## make a nice np.array of shape [nevents,4] (4 being col row asicid and bxid)
## hh is the histo of bxid for each pixel (col,row,asic)
    for velomodule in range (0,52):
        print('Module ...', velomodule)
        with uproot.open(f"/calib/velo/time_alignment/tuples/runs/{runnumber}/ntuple_279986_000.root") as file:
            tree = file.get("VPClusterTuple/ClusterTuple")
            h = tree.arrays(['col','row','asic+3*sensorNb','bxid'],library = 'np',cut = 'module == %d'%(velomodule))
        h1 = np.c_[h['col'],h['row'],h['asic+3*sensorNb'],h['bxid']-bxidreference+20]
        #h1 = np.append(h1,np.c_[h['col'],h['row'],h['asic+3*sensorNb'],h['bxid']-bxidreference2+20],axis=0)
        hh,_axis = np.histogramdd(h1,(np.arange(0,257),np.arange(0,257),np.arange(12+1),np.arange(-bxidrange+20,bxidrange+1+20)))

# now compute the average and stddev for all the pixels
        st = time.time()
        _axisall = np.repeat(np.repeat(np.repeat(_axis[3][:-1].reshape(1,_axis[3][:-1].shape[0]),12,axis= 0).reshape(1,12,_axis[3][:-1].shape[0]),256,axis=0).reshape(1,256,12,_axis[3][:-1].shape[0]),256,axis=0)
        mean_masked = np.ma.average(_axisall, weights = hh, axis = -1)

        en = time.time()
        print(en - st)

        #print(_axisall.shape, mean_masked)
        for sensor in range (0,4):
            for asic in range (0,3):
                #h = np.zeros((256,256))
                h = mean_masked[:,:,asic+sensor*3]
                StringAsic = str((asic + 3*sensor) + 12*velomodule)
                mean = np.round(np.mean(h, dtype = 'float32')-20, 2)
                std = np.round(np.std(h, dtype = 'float32'), 2)
                min = np.round(np.amin(h).astype('float32')-20, 2)
                max = np.round(np.amax(h).astype('float32')-20, 2)
                if not isinstance(mean, np.floating): mean = 0
                if not isinstance(std, np.floating): std = 0
                if not isinstance(min, np.floating): min = 0
                if not isinstance(max, np.floating): max = 0
                to_return['ta'+ StringAsic] = {'asic': (asic + 3*sensor) + 12*velomodule, 'mean':mean, 'std': std, 'min': min, 'max': max}       
    return to_return

if __name__ == '__main__':
    result = TimeAlignmentDict(279986,1903)
    print(result)
    #print(result)
    result['run'] = 279986
    result['uploaded_by'] = 'velo_user'
    result['uploaded_from'] = 'crve02'
    result['comment'] = 'Optimization test A'
    #result['date'] = '--'
    session = requests.Session()
    session.trust_env = False
    content = session.post(
        "{}{}".format('http://127.0.0.1:8003', '/api/ta'),
        data = json.dumps(result), 
        verify = False)
    content.raise_for_status()

#x = TimeAlignmentDict(279986,1903)
#print(x)
#testplotsOneModule(39,2,2,1903,279986)    

    
        
        









