import uproot
import matplotlib.pyplot as plt
import numpy as np
import sys

def get_average(run_number,bxid_min,bxid_max, cut = 0):
    # Get the data from monitoring histogram
    h = uproot.open("/hist/Savesets/ByRun/VeloMon/%s%s0000/%s%s%s000/VeloMon-run%s.root"%(run_number[0],run_number[1],run_number[0],run_number[1],run_number[2],run_number))['VPBunchMonitorsAll/VPClustersPerASICPerBxID;1']
    #h = uproot.open("/hist/Savesets/2023/VELO/VeloMon/05/11/VeloMon-263209-20230511T203220-EOR.root")['VPBunchMonitorsAll_FC/VPClustersPerASICPerBxID;1']
    asics = h.axes[0].centers()
    bxids = h.axes[1].centers()[bxid_min - 1:bxid_max]
    values = h.values()[:, bxid_min - 1:bxid_max] / np.repeat(
        np.sum(h.values()[:, bxid_min - 1:bxid_max], axis=1).reshape(-1, 1), bxids.shape[0], axis=1)
    values[values < cut] = 0
    # Compute the average BxID and stddev for each ASICs
    bxid_values = np.repeat(bxids.reshape(1, bxids.shape[0]), asics.shape[0], axis=0)
    bin_counts  = values.reshape(-1, bxids.shape[0])
    average = np.ma.average( bxid_values , weights=bin_counts, axis=1)
    ## Debugging lines (Pawel):
    # Everything OK
    ###
    print(bxid_values.shape,average.shape)
    std_dev = np.sqrt(np.ma.average( (np.repeat(average.reshape(-1 , 1 ), bxids.shape[0], axis=1)**2 - bxid_values**2) , weights=bin_counts, axis=1))
    return asics,bxids,average,std_dev,values

bxid_min,bxid_max = int(sys.argv[2])-int(sys.argv[3]), int(sys.argv[2])+int(sys.argv[3])
asics,bxids,average,std_dev,values = get_average(sys.argv[1],bxid_min,bxid_max)

compare = False
if len(sys.argv)>4:
    compare = True
    asics1,bxids1,average1,std_dev1,values1 = get_average(sys.argv[4],bxid_min,bxid_max)

plt.subplots(2,1)
plt.subplot(2,1,1)
x, y = np.mgrid[-0.5:asics.shape[0]+0.5,bxids[0]-0.5:bxids[-1]+1.5]
if not compare:
    plt.pcolor(x,y,values,vmax=1)
plt.plot(asics,average,'ko',label = 'run %s'%sys.argv[1])
if compare:
    plt.plot(asics,average1,'ro',label = 'run %s'%sys.argv[4])
    
#plt.plot(asics[std_dev<1.5],average[std_dev<1.5],'ro')
plt.xlabel('ASIC ID')
plt.ylabel('<bxID-bxID$_{bb}$>')
plt.legend()

plt.subplot(2,1,2)
plt.hist(average,bins=np.arange(int(sys.argv[2])-1.,int(sys.argv[2])+1+(2.)/100,2./100),label= 'run: %s, %0.2f +\- %0.4f'%(sys.argv[1],np.mean(average),np.std(average)),histtype= 'step')
if compare:
    plt.hist(average1,bins=np.arange(int(sys.argv[2])-1.,int(sys.argv[2])+1+(2.)/100,2./100),label= 'run: %s, %0.2f +\- %0.4f'%(sys.argv[4],np.mean(average1),np.std(average1)),histtype= 'step')
plt.yscale('log')
#plt.xlabel('$\sigma$(bxID-bxID)$_{bb}$')
plt.legend()
#plt.ylabel('$\sigma$(bxID-bxID)$_{bb}$')



plt.show()
